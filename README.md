# Proyecto del Curso de Node.js: Autenticación, Microservicios y Redis

## Platzi Social

Instruido por:

Carlos Hernández

Descripción:

Haz el backend completo de la red social minimalista PlatziSocial. Crea microservicios con usuarios, posts, follow e interacciones.

<!-- 🚀 Link para ver el proyecto: (https://cripto-exchange-jv.netlify.app/)

(https://www.youtube.com/watch?v=8q7mAVVwNgE)

[![PlatziCriptoExchange](https://i.imgur.com/UnACB8g.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)
[![PlatziCriptoExchange](https://i.imgur.com/JFfurEm.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)
[![PlatziCriptoExchange](https://i.imgur.com/kN3tXin.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)
[![PlatziCriptoExchange](https://i.imgur.com/I3siHaR.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE) -->

## Nota 🏁 (Consiguraciones):

inicializamos:

npm init -y

### Comandos:

- `npm i express`
- `npm i body-parser`
- `npm i nanoid`
- `npm i swagger-ui-express`

nodemon api/index.js

## :+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Youtube: (https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)
